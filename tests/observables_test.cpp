#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "ham_heisenberg.hpp"
#include "observables.hpp"

using ::testing::ElementsAre;

class ObservablesTest : public ::testing::Test{
  protected:
    virtual void SetUp(){}
    virtual void TearDown(){}
};

// print_obs() not tested
TEST_F(ObservablesTest, observables_test){
  Lattice latt("square", 2, 1);
  latt.make_lattice();
  latt.make_lists();
  States states(latt.n_sites);
  states.make_states();
  Hamiltonian ham(1.0, latt, states);
  ham.make_ham();

  Observables obs(1.0, ham.mat);
  obs.calc_obs();
  EXPECT_THAT(obs.energy, -2.7916599753100622);
}