#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include "config.hpp"
#include "lattice.hpp"
#include "states.hpp"
#include "ham_heisenberg.hpp"
#include "observables.hpp"

using std::cout;
using std::endl;
using json = nlohmann::json;

int main(){

  std::ifstream f("../parameters.json");
  json p = json::parse(f);

  Lattice latt(p["lattice"], p["L1"], p["L2"]);
  latt.make_lattice();
  latt.make_lists();
  latt.dump_latt(p["verbose"]);

  States states(latt.n_sites);
  states.make_states();
  //states.print_states();

  Hamiltonian ham(p["J"], latt, states);
  ham.make_ham();

  Observables obs(p["T"], ham.mat);
  obs.calc_obs();
  obs.print_obs();

  return 0;
}
