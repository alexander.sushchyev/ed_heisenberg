#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "states.hpp"

using ::testing::ElementsAre;

class StatesTest : public ::testing::Test{
  protected:
    virtual void SetUp(){}
    virtual void TearDown(){}
};

// print_states() not tested
TEST_F(StatesTest, states_test){
  States states(4);
  states.make_states();
  EXPECT_THAT(states.state_list[0], ElementsAre(0, 0, 0, 0));
  EXPECT_THAT(states.state_list[1], ElementsAre(0, 0, 0, 1));
  EXPECT_THAT(states.state_list[2], ElementsAre(0, 0, 1, 0));
  EXPECT_THAT(states.state_list[3], ElementsAre(0, 0, 1, 1));
  EXPECT_THAT(states.state_list[4], ElementsAre(0, 1, 0, 0));
  EXPECT_THAT(states.state_list[5], ElementsAre(0, 1, 0, 1));
  EXPECT_THAT(states.state_list[6], ElementsAre(0, 1, 1, 0));
  EXPECT_THAT(states.state_list[7], ElementsAre(0, 1, 1, 1));
  EXPECT_THAT(states.state_list[8], ElementsAre(1, 0, 0, 0));
  EXPECT_THAT(states.state_list[9], ElementsAre(1, 0, 0, 1));
  EXPECT_THAT(states.state_list[10], ElementsAre(1, 0, 1, 0));
  EXPECT_THAT(states.state_list[11], ElementsAre(1, 0, 1, 1));
  EXPECT_THAT(states.state_list[12], ElementsAre(1, 1, 0, 0));
  EXPECT_THAT(states.state_list[13], ElementsAre(1, 1, 0, 1));
  EXPECT_THAT(states.state_list[14], ElementsAre(1, 1, 1, 0));
  EXPECT_THAT(states.state_list[15], ElementsAre(1, 1, 1, 1));

  for(int i=0; i<4*4; i++){
    EXPECT_THAT(states.state_list_ind[i], i);
  }
}
