#include "lattice.hpp"
#include "latt_operation.hpp"
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>

using std::sqrt;
using std::cos;
using std::abs;
using std::round;
using std::cout;
using std::cerr;
using std::endl;
using lop::pbc;

Lattice::Lattice(std::string latt_type, int n1, int n2) :
  latt_type(latt_type), n1(n1), n2(n2) {
  // check dimension of the lattice
  assert(n1 > 0 && n2 > 0);
  if(n2==1 && n1!=1){dimension = 1;}
  else if(n1==1 && n2!=1){
    n2 = n1;
    n1 = n2;
    dimension = 1;
  }
  else if(n1 > 1 && n2 > 1){dimension = 2;}

  // lattice specific coordinates
  double a = 1.; // atom distance
  if(latt_type == "square"){
    n_orb = 1;
    if(dimension == 1){n_coord=1;}
    else if(dimension == 2){n_coord = 2;}
    a1[0] = a * 0.; a1[1] = 1.;
    a2[0] = 1.; a2[1] = a * 0.;
    l1 = n1 * a1;
    l2 = n2 * a2;
    orb_pos = std::vector<std::vector<double>>(n_orb,std::vector<double>(2,0));
    orb_pos[0][0] = 0.;
    orb_pos[0][1] = 0.;
  }
  else if(latt_type == "honeycomb"){
    assert(dimension == 2);
    n_orb = 2;
    n_coord = 3;
    a1[0] = a * 3./2.; a1[1] = a * sqrt(3.)/2.;
    a2[0] = a * 3./2.; a2[1] = a * -sqrt(3.)/2;
    l1 = n1 * a1;
    l2 = n2 * a2;
    orb_pos = std::vector<std::vector<double>>(n_orb,std::vector<double>(2,0));
    orb_pos[0][0] = (a1[0] + a2[0]) * 1./3.;
    orb_pos[0][1] = (a1[1] + a2[1]) * 1./3.;
    orb_pos[1][0] = (a1[0] + a2[0]) * 2./3.;
    orb_pos[1][1] = (a1[1] + a2[1]) * 2./3.;
  }
  else{
    cerr << "Lattice not implemented" << endl;
  }
}

Lattice::~Lattice(){}

void Lattice::make_lattice(){
  int ldim1, ldim2;
  double x, pi = acos(-1.), eta=1e-8;
  Eigen::Vector2d ap, bp, dp, xp, x1p, xkp;
  Eigen::Matrix2d mat, mat_inv;

  mat << a1[0], a1[1], a2[0], a2[1];
  mat_inv = 2.*pi*mat.inverse();
  bz1[0] = mat_inv(0,0);
  bz1[1] = mat_inv(1,0);
  bz2[0] = mat_inv(0,1);
  bz2[1] = mat_inv(1,1);

  x = abs(2.*pi / (bz1.dot(l1)*bz2.dot(l2) - bz2.dot(l1)*bz1.dot(l2)));
  b1 = x * (bz2.dot(l2)*bz1 - bz1.dot(l2)*bz2);
  b2 = x * (bz1.dot(l1)*bz2 - bz2.dot(l1)*bz1);

  // count number of unit cells (brav. lattice sites)
  ldim1 = abs(int(round(bz1.dot(l1)/2./pi)));
  ldim2 = abs(int(round(bz2.dot(l1)/2./pi)));
  if(ldim2 > ldim1){ldim1 = ldim2;}
  ldim2 = abs(int(round(bz1.dot(l2)/2./pi)));
  if(ldim2 > ldim1){ldim1 = ldim2;}
  ldim2 = abs(int(round(bz2.dot(l2)/2./pi)));
  if(ldim2 > ldim1){ldim1 = ldim2;}
  int lf, lq, nc = 0;
  for(int i1=-ldim1; i1 <= ldim1; i1++){
    for(int i2=-ldim1; i2 <= ldim1; i2++){
      xp = i1*a1 + i2*a2;
      lf = 1;
      for(int i=0; i<4; i++){
        if(i==0){ap = l2;}
        if(i==1){ap = l1;}
        if(i==2){ap = l2 - l1;}
        if(i==3){ap = l2 + l1;}
        if(xp.dot(ap) <= ap.squaredNorm()/2.+eta &&
        xp.dot(ap) >= -ap.squaredNorm()/2.+eta){
          lf *= 1;
        }
        else{
          lf = 0;
        }
      }
      if(lf == 1){nc += 1;}
    }
  }
  lq = nc;
  n_ucells = lq;
  n_sites = n_ucells * n_orb;

  // write bravais lattice lists
  c_list = std::vector<std::vector<int>>(n_ucells,std::vector<int>(2,0));
  nc = 0;
  for(int i1=-ldim1; i1 <= ldim1; i1++){
    for(int i2=-ldim1; i2 <= ldim1; i2++){
      xp = i1*a1 + i2*a2;
      lf = 1;
      for(int i=0; i<4; i++){
        if(i==0){ap = l2;}
        if(i==1){ap = l1;}
        if(i==2){ap = l2 - l1;}
        if(i==3){ap = l2 + l1;}
        if(xp.dot(ap) <= ap.squaredNorm()/2.+eta &&
        xp.dot(ap) >= -ap.squaredNorm()/2.+eta){
          lf *= 1;
        }
        else{
          lf = 0;
        }
      }
      if(lf == 1){
        c_list[nc][0] = i1;
        c_list[nc][1] = i2;
        c_invlist[i1][i2] = nc;
        nc++;
      }
    }
  }

  // write k space lists
  c_listk = std::vector<std::vector<int>>(n_ucells,std::vector<int>(2,0));
  nc = 0;
  for(int i1=-ldim1; i1 <= ldim1; i1++){
    for(int i2=-ldim1; i2 <= ldim1; i2++){
      xkp = i1*b1 + i2*b2;
      lf = 1;
      for(int i=0; i<4; i++){
        if(i==0){bp = bz2;}
        if(i==1){bp = bz1;}
        if(i==2){bp = bz2 - bz1;}
        if(i==3){bp = bz2 + bz1;}
        if(xkp.dot(bp) <= bp.squaredNorm()/2.+eta &&
        xkp.dot(bp) >= -bp.squaredNorm()/2.+eta){
          lf *= 1;
        }
        else{
          lf = 0;
        }
      }
      if(lf == 1){
        c_listk[nc][0] = i1;
        c_listk[nc][1] = i2;
        c_invlistk[i1][i2] = nc;
        nc++;
      }
    }
  }

  // write nearest neigbor list
  int nnr, nnr1, nnr2;
  for(unsigned int nr=0; nr<n_ucells; nr++){
    for(int i1=-1; i1 <= 1; i1++){
      for(int i2=-1; i2 <= 1; i2++){
        dp = i1*a1 + i2*a2;
        xp = c_list[nr][0]*a1 + c_list[nr][1]*a2 + dp;
        x1p = pbc(xp, l1, l2);
        xp = pbc(x1p, l1, l2);
        x1p = pbc(xp, l1, l2);
        xp = pbc(x1p, l1, l2);
        nnr1 = int(round(bz1.dot(xp)/2./pi));
        nnr2 = int(round(bz2.dot(xp)/2./pi));
        nnr = c_invlist[nnr1][nnr2];
        c_nnlist[nr][i1][i2] = nnr;
      }
    }
  }
}

void Lattice::make_lists(){
  Eigen::Vector2d v;
  list = std::vector<std::vector<int>>(n_sites,std::vector<int>(2,0));
  invlist = std::vector<std::vector<int>>(n_ucells,std::vector<int>(n_orb,0));
  if(latt_type == "square"){
    bond_list = std::vector<std::vector<int>>(n_sites*n_coord,std::vector<int>(2,0));
  }
  else if(latt_type == "honeycomb"){
    bond_list = std::vector<std::vector<int>>(n_sites*n_coord/2,std::vector<int>(2,0));
  }
  bond_invlist = std::vector<std::vector<int>>(n_sites,std::vector<int>(n_coord,0));
  int nn_ind, nn_uc, s_ind, nc = 0;

  for(unsigned int i=0; i<n_ucells; i++){
    for(unsigned int j=0; j<n_orb; j++){
      list[nc][0] = i; // unit cell index
      list[nc][1] = j; // orbital index
      invlist[i][j] = nc; // consequent site index
      nc++;
    }
  }
  nc = 0;
  for(unsigned int i=0; i<n_ucells; i++){
    s_ind = invlist[i][0]; // avoid double counting
    for(unsigned int k=0; k<n_coord; k++){
      if(latt_type == "square"){
        if(k == 0){nn_uc = c_nnlist[i][1][0];}
        else if(k == 1){nn_uc = c_nnlist[i][0][1];}
        nn_ind = invlist[nn_uc][0];
        if(s_ind != nn_ind){
          bond_list[nc][0] = s_ind;
          bond_list[nc][1] = nn_ind;
          bond_invlist[s_ind][k] = nc;
          nc++;
        }
      }
      else if(latt_type == "honeycomb"){
        if(k == 0){nn_uc = i;}
        else if(k == 1){nn_uc = c_nnlist[i][0][-1];}
        else if(k == 2){nn_uc = c_nnlist[i][-1][0];}
        nn_ind = invlist[nn_uc][1];
        if(s_ind != nn_ind){
          bond_list[nc][0] = s_ind;
          bond_list[nc][1] = nn_ind;
          bond_invlist[s_ind][k] = nc;
          nc++;
        }
      }
    }
  }
}

void Lattice::dump_latt(short int verbose){
  int nnr;
  Eigen::Vector2d rs, ks, nn;

  if(verbose == 1){
    cout << "Bravais lattice" << endl;
    for(unsigned int i=0; i<n_ucells; i++){
      rs = c_list[i][0]*a1 + c_list[i][1]*a2;
      cout << i << std::setw(10) << rs[0] << "," << std::setw(10) << rs[1] << endl;
    }
    cout << "real space lattice" << endl;
    for(unsigned int i=0; i<n_ucells; i++){
      for(unsigned int j=0; j<n_orb; j++){
        rs = c_list[i][0]*a1 + c_list[i][1]*a2;
        rs[0] += orb_pos[j][0];
        rs[1] += orb_pos[j][1];
        cout << invlist[i][j] << std::setw(10) << rs[0] << "," << std::setw(10) << rs[1] << endl;
      }
    }
    cout << "Reciprocal lattice" << endl;
    for(unsigned int i=0; i<n_ucells; i++){
      ks = c_listk[i][0]*b1 + c_listk[i][1]*b2;
      cout << i << std::setw(10) << ks[0] << "," << std::setw(10) << ks[1] << endl;
    }
  }
  else if(verbose == 2){
    cout << "nearest neighbors" << endl;
    for(unsigned int nr=0; nr<n_ucells; nr++){
      rs = c_list[nr][0]*a1 + c_list[nr][1]*a2;
      cout << "i: " << rs[0] << "," << rs[1] << endl;
      for(int nd1=-1; nd1 <= 1; nd1++){
        if(dimension == 2){
          for(int nd2=-1; nd2 <= 1; nd2++){
            nn = nd1*a1 + nd2*a2;
            nnr = c_nnlist[nr][nd1][nd2];
            rs = c_list[nnr][0]*a1 + c_list[nnr][1]*a2;
            cout << "i+(" << nn[0] << "," << nn[1] << ")=" << rs[0] << "," << rs[1] << endl;
          }
        }
        else if(dimension == 1){
          nn = nd1*a1;
          nnr = c_nnlist[nr][nd1][0];
          rs = c_list[nnr][0]*a1;
          cout << "i+(" << nn[0] << "," << nn[1] << ")=" << rs[0] << "," << rs[1] << endl;
        }
      }
    }
  }
  else{
    return;
  }
}
