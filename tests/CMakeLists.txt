set(SOURCES_TEST test.cpp 
  lattice_test.cpp 
  states_test.cpp 
  ham_heisenberg_test.cpp 
  observables_test.cpp  
)

add_executable(ed_test ${SOURCES_TEST})

target_include_directories(ed_test PUBLIC
  "${CMAKE_CURRENT_BINARY_DIR}"
  "${CMAKE_SOURCE_DIR}/lib"
  "${CMAKE_SOURCE_DIR}/src"
)

target_link_libraries(ed_test
  gtest
  gmock
  main_lib
)
