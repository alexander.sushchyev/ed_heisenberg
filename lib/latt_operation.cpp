#include <iostream>
#include "latt_operation.hpp"

namespace lop{
  Eigen::Vector2d pbc(Eigen::Vector2d n_p, Eigen::Vector2d l1_p,
  Eigen::Vector2d l2_p){
    Eigen::Vector2d nr_p, x_p;
    double x, eta=1e-8;

    nr_p = n_p;
    for(int i=0; i<4; i++){
      if(i==0){x_p = l2_p;}
      if(i==1){x_p = l1_p;}
      if(i==2){x_p = l2_p - l1_p;}
      if(i==3){x_p = l2_p + l1_p;}
      x = nr_p.dot(x_p) / x_p.squaredNorm();
      if(x >= 0.5+eta){nr_p -= x_p;}
      if(x <= -0.5+eta){nr_p += x_p;}
    }
    return nr_p;
  }
}
