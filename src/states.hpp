#pragma once
#include <vector>

class States{
  private:
    int n_spins, n_states;
    std::vector<int> init_state; // 1 = up, 0 = down spin
  public:
    std::vector<std::vector<int>> state_list;
    std::vector<unsigned long int> state_list_ind;
    States(int n_spins);
    void make_states();
    void print_states();
};
