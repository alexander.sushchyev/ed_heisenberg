#include "mat_operation.hpp"
#include <Eigen/Eigenvalues>
#include <iostream>

namespace mop{
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> diagonalization(Eigen::MatrixXd &A){
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es(A);
    return es;
  }
  Eigen::MatrixXd eigenvectors(Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es){
    return es.eigenvectors();
  }
  Eigen::VectorXd eigenvalues(Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es){
    return es.eigenvalues();
  }
}
