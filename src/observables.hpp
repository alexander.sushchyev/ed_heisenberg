#include "states.hpp"
#include <Eigen/Dense>
#include <vector>

class Observables{
  private:
    double T;
    Eigen::MatrixXd ham, eigenvectors;
    Eigen::VectorXd eigenvalues;
  public:
    double energy;
    Observables(double T, Eigen::MatrixXd &A);
    void calc_obs();
    void print_obs();
};
