#pragma once
#include <Eigen/Dense>

namespace mop{
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> diagonalization(Eigen::MatrixXd &A);
  Eigen::MatrixXd eigenvectors(Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es);
  Eigen::VectorXd eigenvalues(Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es);
}
