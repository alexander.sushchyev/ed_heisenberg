#pragma once
#include <Eigen/Dense>

namespace lop{
  Eigen::Vector2d pbc(Eigen::Vector2d n_p, Eigen::Vector2d l1_p,
    Eigen::Vector2d l2_p);
}
