#pragma once
#include <vector>
#include <cassert>

namespace sop{
  void permute_state(int *a, int len);
  void print_arr(int *a, int len);
  void print_vec(std::vector<int> &a);
  bool comp_states(std::vector<int> &a, std::vector<int> &b);
}
