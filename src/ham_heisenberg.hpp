#include "lattice.hpp"
#include "states.hpp"
#include <Eigen/Dense>
#include <vector>

class Hamiltonian{
  private:
    double J;
    long unsigned int mat_dim; // matrix of size dim x dim
    std::vector<std::vector<int>> bond_list;
    std::vector<std::vector<int>> state_list; // list of states in z-basis
    double sz(int &spin_orientation); // EV. to a (1 particle) state in z basis
    int s_up(int &spin_orientation); // 1 part. spin raising op.; 0->1 (down -> up)
    int s_do(int &spin_orientation); // 1 part. spin lowering op.; 1->0 (up -> down)
  public:
    friend class Lattice;
    friend class States;
    Eigen::MatrixXd mat;
    Hamiltonian(double J, Lattice &latt, States &states);
    void make_ham();
};
