#include "states.hpp"
#include "state_operation.hpp"
#include <iostream>
#include <cmath>
#include <bitset>

using std::cout;
using std::endl;
using std::pow;
using std::string;
using std::bitset;
using sop::permute_state;
using sop::print_arr;
using sop::print_vec;

States::States(int n_spins_) : n_spins(n_spins_) {
  n_states = pow(2, n_spins); // total number of states
  init_state = std::vector<int>(n_spins, 0); // all spins down
}

void States::make_states(){
  std::vector<int> tmp_vec(n_spins, 0);
  int tmp_arr[n_spins];
  string binary;
  unsigned long int decimal;

  for(int i=0; i<n_spins; i++){
    tmp_arr[i] = init_state[i];
  }
  int n = sizeof(tmp_arr) / sizeof(tmp_arr[0]);
  for(int i=0; i<(1 << n); i++){
    //print_arr(tmp_arr, n)
    for(int i=0; i<n_spins; i++){
      tmp_vec[i] = tmp_arr[i];
    }
    state_list.push_back(tmp_vec);
    permute_state(tmp_arr, n);
  }

  for(auto state : state_list){
    binary = "";
    for(unsigned long int j=0; j<state.size(); j++){
      binary += std::to_string(state[j]);
    }
    decimal = bitset<32>(binary).to_ulong();
    state_list_ind.push_back(decimal);
  }
}

void States::print_states(){
  for(auto state : state_list){
    print_vec(state);
  }
}
