#include "state_operation.hpp"
#include <iostream>

namespace sop{
  void permute_state(int *a, int len){
    int carry = 1;
    for (int i = len - 1; carry > 0 && i >= 0; i--){
      int result = a[i] + carry;
      carry = result >> 1;
      a[i] = result & 1;
    }
  }

  void print_arr(int *a, int len){
    printf("[");
    for (int i = 0; i < len; i++){
      if (i > 0) printf(",");
      printf("%d", a[i]);
    }
    printf("]\n");
  }

  void print_vec(std::vector<int> &a){
    printf("[");
    for(long unsigned int i=0; i<a.size(); i++){
      if (i > 0) printf(",");
      printf("%d", a[i]);
    }
    printf("]\n");
  }

  bool comp_states(std::vector<int> &a, std::vector<int> &b){
    assert(a.size() == b.size());
    bool comp = (a == b) ? true : false;
    return comp;
  }
}
