#include "observables.hpp"
#include "mat_operation.hpp"
#include <cmath>
#include <iostream>

using std::cout;
using std::endl;
using std::exp;

Observables::Observables(double T, Eigen::MatrixXd &A) : T(T), ham(A) {
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es;
  es = mop::diagonalization(ham);
  eigenvectors = mop::eigenvectors(es);
  eigenvalues = mop::eigenvalues(es);
}

void Observables::calc_obs(){
  double ener=0, part_sum=0;
  double beta = 1./T;
  for(double eigenval : eigenvalues){
    part_sum += exp(-beta*eigenval);
    ener += eigenval*exp(-beta*eigenval);
  }
  energy = ener/part_sum;
}

void Observables::print_obs(){
  cout << "Energy: " << energy << endl;
}
