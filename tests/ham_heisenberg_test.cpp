#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "ham_heisenberg.hpp"

using ::testing::ElementsAre;

class HamHeisenbergTest : public ::testing::Test{
  protected:
    virtual void SetUp(){}
    virtual void TearDown(){}
};

TEST_F(HamHeisenbergTest, hamiltonian){
    Lattice latt("square", 2, 1);
    latt.make_lattice();
    latt.make_lists();
    States states(latt.n_sites);
    states.make_states();

    Hamiltonian ham(1., latt, states);
    ham.make_ham();

    EXPECT_THAT(ham.mat(0,0), 1);
    EXPECT_THAT(ham.mat(0,1), 0);
    EXPECT_THAT(ham.mat(0,2), 0);
    EXPECT_THAT(ham.mat(0,3), 0);
    EXPECT_THAT(ham.mat(1,0), 0);
    EXPECT_THAT(ham.mat(1,1), -1);
    EXPECT_THAT(ham.mat(1,2), 2);
    EXPECT_THAT(ham.mat(1,3), 0);
    EXPECT_THAT(ham.mat(2,0), 0);
    EXPECT_THAT(ham.mat(2,1), 2);
    EXPECT_THAT(ham.mat(2,2), -1);
    EXPECT_THAT(ham.mat(2,3), 0);
    EXPECT_THAT(ham.mat(3,0), 0);
    EXPECT_THAT(ham.mat(3,1), 0);
    EXPECT_THAT(ham.mat(3,2), 0);
    EXPECT_THAT(ham.mat(3,3), 1);
}