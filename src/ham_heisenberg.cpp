#include "ham_heisenberg.hpp"
#include "state_operation.hpp"
#include <iostream>

using std::cout;
using std::endl;
using sop::comp_states;

Hamiltonian::Hamiltonian(double J, Lattice& latt, States& states) :
  J(J), bond_list(latt.bond_list), state_list(states.state_list){
    mat_dim = state_list.size();
  }

double Hamiltonian::sz(int &spin_orientation){
  double eigenvalue = (spin_orientation) ? 0.5 : -0.5;
  return eigenvalue;
}

int Hamiltonian::s_up(int &spin_orientation){
  // raising m=+1/2 allowed: introduce some break condition...?
  int spin_orientation_new = spin_orientation + 1;
  return spin_orientation_new;
}

int Hamiltonian::s_do(int &spin_orientation){
  // same as s_up
  int spin_orientation_new = spin_orientation - 1;
  return spin_orientation_new;
}

void Hamiltonian::make_ham(){
  mat = Eigen::MatrixXd::Zero(mat_dim, mat_dim);
  for(long unsigned int n=0; n<mat_dim; n++){ // loop over state ind.
    std::vector<int> ket = state_list[n];
    for(long unsigned int m=0; m<=n; m++){ // matrix is symmetric
      std::vector<int> bra = state_list[m];
      /*cout << "mat elem " << n << " " << m << endl;
      cout << "bra "; for(auto i : bra){cout << i << " ";} cout << "\n";
      cout << "ket "; for(auto i : ket){cout << i << " ";} cout << "\n";*/
      for(std::vector<int> bond : bond_list){
        if(n==m){
          // sz_i * sz_j; bra == ket
          mat(n,m) += 2.*J*sz(ket[bond[0]])*sz(ket[bond[1]]);
        }
      else{
        // operators act on ket; produces unphysical tmp states...
        std::vector<int> ket_tmp1 = ket, ket_tmp2 = ket;
        ket_tmp1[bond[0]] = J*s_up(ket[bond[0]]); // s+_i
        ket_tmp1[bond[1]] = J*s_do(ket[bond[1]]); // s-_j
        ket_tmp2[bond[0]] = J*s_do(ket[bond[0]]); // s-_i
        ket_tmp2[bond[1]] = J*s_up(ket[bond[1]]); // s+_j
        if(comp_states(bra, ket_tmp1)){
          mat(n,m) += J;
          mat(m,n) += J;
          }
        if(comp_states(bra, ket_tmp2)){
          mat(n,m) += J;
          mat(m,n) += J;
          }
        /*cout << "bond " << bond[0] << " " << bond[1] << endl;
        cout << "si+sj- "; for(auto i : ket_tmp1){cout << i << " ";} cout << "\n";
        cout << "si-sj+ "; for(auto i : ket_tmp2){cout << i << " ";} cout << "\n";*/
        }
      }
    }
  }
  //cout << mat << endl;
}
