#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "lattice.hpp"

using ::testing::ElementsAre;

class LatticeTest : public ::testing::Test{
  protected:
    virtual void SetUp(){}
    virtual void TearDown(){}
};

// test void methods by expecting the correct member variables
// dump(), bond_invlist left out
TEST_F(LatticeTest, lattice_square){
  Lattice latt("square", 2, 2);
  EXPECT_EQ(1, latt.n_orb);
  EXPECT_EQ(2, latt.n_coord);
  latt.make_lattice();
  EXPECT_EQ(4, latt.n_sites);
  EXPECT_EQ(4, latt.n_ucells);

  EXPECT_THAT(latt.c_list[0], ElementsAre(0, 0));
  EXPECT_THAT(latt.c_list[1], ElementsAre(0, 1));
  EXPECT_THAT(latt.c_list[2], ElementsAre(1, 0));
  EXPECT_THAT(latt.c_list[3], ElementsAre(1, 1));

  EXPECT_THAT(latt.c_invlist[0][0], 0);
  EXPECT_THAT(latt.c_invlist[0][1], 1);
  EXPECT_THAT(latt.c_invlist[1][0], 2);
  EXPECT_THAT(latt.c_invlist[1][1], 3);

  EXPECT_THAT(latt.c_listk[0], ElementsAre(0, 0));
  EXPECT_THAT(latt.c_listk[1], ElementsAre(0, 1));
  EXPECT_THAT(latt.c_listk[2], ElementsAre(1, 0));
  EXPECT_THAT(latt.c_listk[3], ElementsAre(1, 1));

  EXPECT_THAT(latt.c_invlistk[0][0], 0);
  EXPECT_THAT(latt.c_invlistk[0][1], 1);
  EXPECT_THAT(latt.c_invlistk[1][0], 2);
  EXPECT_THAT(latt.c_invlistk[1][1], 3);

  int nn_arr[36] = {3, 2, 3, 1, 0, 1, 3, 2, 3, 2, 3, 2, 0, 1, 0, 2, 3, 2, 1, 0, 1, 3, 2, 3, 1, 0, 1, 0, 1, 0, 2, 3, 2, 0, 1, 0};
  int count = 0;
  for(int nr=0; nr<4; nr++){
    for(int i1=-1; i1 <= 1; i1++){
      for(int i2=-1; i2 <= 1; i2++){
        EXPECT_THAT(latt.c_nnlist[nr][i1][i2], nn_arr[count]);
        count++;
      }
    }
  }
  
  latt.make_lists();
  for(int i=0; i<4; i++){
    EXPECT_THAT(latt.list[i], ElementsAre(i, 0));  
  }

  for(int i=0; i<4; i++){
    EXPECT_THAT(latt.invlist[i][0], i);  
  }

  EXPECT_THAT(latt.bond_list[0][1], 2);
  EXPECT_THAT(latt.bond_list[1][1], 1);
  EXPECT_THAT(latt.bond_list[2][1], 3);
  EXPECT_THAT(latt.bond_list[3][1], 0);
  EXPECT_THAT(latt.bond_list[4][1], 0);
  EXPECT_THAT(latt.bond_list[5][1], 3);
  EXPECT_THAT(latt.bond_list[6][1], 1);
  EXPECT_THAT(latt.bond_list[7][1], 2);
}


TEST_F(LatticeTest, lattice_honeycomb){
  Lattice latt("honeycomb", 2, 2);
  EXPECT_EQ(2, latt.n_orb);
  EXPECT_EQ(3, latt.n_coord);
  latt.make_lattice();
  EXPECT_EQ(8, latt.n_sites);
  EXPECT_EQ(4, latt.n_ucells);

  EXPECT_THAT(latt.c_list[0], ElementsAre(-1, 1));
  EXPECT_THAT(latt.c_list[1], ElementsAre(0, 0));
  EXPECT_THAT(latt.c_list[2], ElementsAre(0, 1));
  EXPECT_THAT(latt.c_list[3], ElementsAre(1, 0));

  EXPECT_THAT(latt.c_invlist[-1][1], 0);
  EXPECT_THAT(latt.c_invlist[0][0], 1);
  EXPECT_THAT(latt.c_invlist[0][1], 2);
  EXPECT_THAT(latt.c_invlist[1][0], 3);

  EXPECT_THAT(latt.c_listk[0], ElementsAre(0, 0));
  EXPECT_THAT(latt.c_listk[1], ElementsAre(0, 1));
  EXPECT_THAT(latt.c_listk[2], ElementsAre(1, 0));
  EXPECT_THAT(latt.c_listk[3], ElementsAre(1, 1));

  EXPECT_THAT(latt.c_invlistk[0][0], 0);
  EXPECT_THAT(latt.c_invlistk[0][1], 1);
  EXPECT_THAT(latt.c_invlistk[1][0], 2);
  EXPECT_THAT(latt.c_invlistk[1][1], 3);

  int nn_arr[36] = {1, 2, 1, 3, 0, 3, 1, 2, 1, 0, 3, 0, 2, 1, 2, 0, 3, 0, 3, 0, 3, 1, 2, 1, 3, 0, 3, 2, 1, 2, 0, 3, 0, 2, 1, 2};
  int count = 0;
  for(int nr=0; nr<4; nr++){
    for(int i1=-1; i1 <= 1; i1++){
      for(int i2=-1; i2 <= 1; i2++){
        EXPECT_THAT(latt.c_nnlist[nr][i1][i2], nn_arr[count]);
        count++;
      }
    }
  }

  latt.make_lists();
  for(int i=0; i<4; i++){
    EXPECT_THAT(latt.list[2*i], ElementsAre(i, 0));
    EXPECT_THAT(latt.list[2*i+1], ElementsAre(i, 1));  
  }

  for(int i=0; i<8; i++){
    EXPECT_THAT(latt.invlist[std::floor(i/2)][i%2], i);  
  }

  EXPECT_THAT(latt.bond_list[0][1], 1);
  EXPECT_THAT(latt.bond_list[1][1], 7);
  EXPECT_THAT(latt.bond_list[2][1], 5);
  EXPECT_THAT(latt.bond_list[3][1], 3);
  EXPECT_THAT(latt.bond_list[4][1], 5);
  EXPECT_THAT(latt.bond_list[5][1], 7);
  EXPECT_THAT(latt.bond_list[6][1], 5);
  EXPECT_THAT(latt.bond_list[7][1], 3);
  EXPECT_THAT(latt.bond_list[8][1], 1);
  EXPECT_THAT(latt.bond_list[9][1], 7);
  EXPECT_THAT(latt.bond_list[10][1], 1);
  EXPECT_THAT(latt.bond_list[11][1], 3);
}
