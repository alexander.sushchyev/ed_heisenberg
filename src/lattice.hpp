#pragma once
#include <Eigen/Dense>
#include <string>
#include <vector>
#include <unordered_map>
#include <tuple>

class Lattice{
  private:
    std::string latt_type;
    unsigned int n1, n2; // number of u_cells along latt. vec.
    unsigned int dimension;
    Eigen::Vector2d a1, a2, l1, l2; // latt. vec., simulation cell topology
    Eigen::Vector2d b1, b2, bz1, bz2; // rec. vec., BZ vec
    std::vector<std::vector<double>> orb_pos; // orbital positions in u_cell
  public:
    unsigned int n_ucells, n_orb, n_coord, n_sites, n_bonds;
    std::vector<std::vector<int>> list; // [ucell], [orb]
    std::vector<std::vector<int>> invlist; // [ucell][orb], site index list
    std::vector<std::vector<int>> bond_list; // [site], [neigbor]
    std::vector<std::vector<int>> bond_invlist; // [site][neigbor], bond index list
    std::vector<std::vector<int>> c_list; // ucells pos.
    std::unordered_map<int,std::unordered_map<int,int>> c_invlist, c_invlistk;  // i1*a1+i2*a2
    // nearest neigbor index
    std::unordered_map<int, std::unordered_map<int, std::unordered_map<int,int>>> c_nnlist;
    std::vector<std::vector<int>> c_listk; // reciprocal latt
    Lattice(std::string latt_type, int nx, int ny);
    ~Lattice();
    void make_lattice();
    void make_lists();
    void dump_latt(short int verbose);
};
