# Exact Diagonalization #

[![pipeline status](https://git.rwth-aachen.de/alexander.sushchyev/ed_heisenberg/badges/master/pipeline.svg)](https://git.rwth-aachen.de/alexander.sushchyev/ed_heisenberg/-/commits/master)

[![Latest Release](https://git.rwth-aachen.de/alexander.sushchyev/ed_heisenberg/-/badges/release.svg)](https://git.rwth-aachen.de/alexander.sushchyev/ed_heisenberg/-/releases)

This is a C++ code for exact diagonalization of the Heisenberg model.
The modular structure allows for easy additions of different basis state sets as well models.

## Installation ##

The code is supposed to be used from a command line, e.g. in Linux.
To build the executable and testsuite type:
```
mkdir build
cd build
cmake ..
make
```

## Usage ##
In the **parameters.json** file we determin the *lattice* and its size in x and y direction, *verbosity* of the stdout 
(0: no output, 1: real space lattice and k-space lattice, 2: nearest neighbors). Furthermore, the coupling strength *J* and the
temperature *T* for the thermal expectation values.
To run the program simply type `./main` in the *build* directory, to execute the testsuite type `./ed_test`.
